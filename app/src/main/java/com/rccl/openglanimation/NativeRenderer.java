package com.rccl.openglanimation;

import android.opengl.GLSurfaceView;
import android.support.annotation.NonNull;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 11/02/2017.
 */
public class NativeRenderer implements GLSurfaceView.Renderer {
    static {
        System.loadLibrary("native-lib");
    }

    private String waterModel;
    private String waterTextures;
    private String cruiseModel;
    private String cruiseTexture;
    private boolean isReady;

    public void setWaterPaths(@NonNull String model, @NonNull String texture) {
        waterModel = model;
        waterTextures = texture;
    }

    public void setCruisePaths(@NonNull String model, @NonNull String texture) {
        cruiseModel = model;
        cruiseTexture = texture;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        initOpenGL();
        loadWater(waterModel, waterTextures);
        loadShip(cruiseModel, cruiseTexture);
        isReady = true;
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        setViewPort(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        draw();
    }

    public void onDestroy() {
        releaseResources();
    }

    public void onClick(float x, float y) {
        if (isReady) {
            click(x, y);
        }
    }

    public native void initOpenGL();

    public native void setViewPort(int width, int height);

    public native boolean loadWater(String modelPath, String texturePath);

    public native boolean loadShip(String modelPath, String texturePath);

    public native void draw();

    public native int click(float x, float y);

    public native void releaseResources();
}
