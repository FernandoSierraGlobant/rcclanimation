//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#ifndef RCCLANIMATION_TEXTUREMANAGER_H
#define RCCLANIMATION_TEXTUREMANAGER_H

#include <map>
#include <vector>
#include "../../PlatformProvider.h"
#include "OpenGLUtils.h"
#include "../../libs/soil/SOIL.h"

using namespace std;

class TextureManager {
private:
    static map<int, vector<GLuint>> texturesMap;
public:
    static void loadTextures(int objectId, vector<const char *> textures) {
        vector<GLuint> texturesList = texturesMap[objectId];
        if (texturesList.empty()) {
            for (const char *texture : textures) {
                GLuint textureId = SOIL_load_OGL_texture(texture,
                                                         SOIL_LOAD_AUTO,
                                                         SOIL_CREATE_NEW_ID,
                                                         SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
                                                         SOIL_FLAG_NTSC_SAFE_RGB |
                                                         SOIL_FLAG_COMPRESS_TO_DXT);
                if (0 == textureId) {
                    logger->log(Logger::ERROR, SOIL_last_result());
                }
                if (textureId > 0) {
                    texturesList.push_back(textureId);
                }
            }
            texturesMap[objectId] = texturesList;
        }
    }

    static vector<GLuint> getTexturesList(int objectId) { return texturesMap[objectId]; }

    static void clear(int objectId) {
        for (GLuint id : texturesMap[objectId]) {
            glDeleteTextures(1, &id);
        }
    }
};

#endif //RCCLANIMATION_TEXTUREMANAGER_H
