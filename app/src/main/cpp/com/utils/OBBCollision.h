//
// Created by jesus.sierra on 22/02/17.
//

#ifndef RCCLANIMATION_OBBCOLLISION_H
#define RCCLANIMATION_OBBCOLLISION_H

#include "../../libs/glm/glm.hpp"

using namespace glm;

class OBBCollision {
public:
    static void screenPositionToWorldRay(
            float touchX,
            float touchY,
            int screenWidth,
            int screenHeight,
            mat4 viewMatrix,
            mat4 projectionMatrix,
            vec3 &outOrigin,
            vec3 &outDirection) {
        // NDC -> Normalized Device Coordinates
        float normalizedX = (touchX / (float) screenWidth - 0.5f) * 2.0f;
        float normalizedY = (touchY / (float) screenHeight - 0.5f) * 2.0f;
        vec4 rayStartNDC(normalizedX, normalizedY, -1.0f, 1.0f);
        vec4 rayEndNDC(normalizedX, normalizedY, 0.0f, 1.0f);
        // Invert projection and view matrices to get the ray vector and direction
        mat4 invertedMatrix = inverse(projectionMatrix * viewMatrix);
        vec4 worldRayStart = invertedMatrix * rayStartNDC;
        worldRayStart /= worldRayStart.w;
        vec4 worldRayEnd = invertedMatrix * rayEndNDC;
        worldRayEnd /= worldRayEnd.w;
        vec3 rayDirection(worldRayEnd - worldRayStart);
        rayDirection = normalize(rayDirection);
        // Put ray's vector and direction to the output values
        outOrigin = vec3(worldRayStart);
        outDirection = normalize(rayDirection);
    }

    static bool testRayObbIntersection(
            vec3 rayOrigin,
            vec3 rayDirection,
            vec3 aabb_min,
            vec3 aabb_max,
            mat4 modelMatrix,
            float &intersectionDistance) {

        float tMin = 0.0f;
        float tMax = 100000.0f;
        vec3 obbPositionWorldSpace(modelMatrix[3].x, modelMatrix[3].y, modelMatrix[3].z);
        vec3 delta = obbPositionWorldSpace - rayOrigin;

        // Test intersection with the 2 planes perpendicular to the OBB's X axis
        {
            vec3 xAxis(modelMatrix[0].x, modelMatrix[0].y, modelMatrix[0].z);
            float e = dot(xAxis, delta);
            float f = dot(rayDirection, xAxis);

            if (fabs(f) > 0.001f) { // Standard case

                float t1 = (e + aabb_min.x) / f; // Intersection with the "left" plane
                float t2 = (e + aabb_max.x) / f; // Intersection with the "right" plane
                // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

                // We want t1 to represent the nearest intersection,
                // so if it's not the case, invert t1 and t2
                if (t1 > t2) {
                    float w = t1;
                    t1 = t2;
                    t2 = w; // swap t1 and t2
                }

                // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
                if (t2 < tMax)
                    tMax = t2;
                // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
                if (t1 > tMin)
                    tMin = t1;

                // And here's the trick :
                // If "far" is closer than "near", then there is NO intersection.
                // See the images in the tutorials for the visual explanation.
                if (tMax < tMin)
                    return false;

            } else { // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
                if (-e + aabb_min.x > 0.0f || -e + aabb_max.x < 0.0f)
                    return false;
            }
        }


        // Test intersection with the 2 planes perpendicular to the OBB's Y axis
        // Exactly the same thing than above.
        {
            vec3 yAxis(modelMatrix[1].x, modelMatrix[1].y, modelMatrix[1].z);
            float e = dot(yAxis, delta);
            float f = dot(rayDirection, yAxis);

            if (fabs(f) > 0.001f) {

                float t1 = (e + aabb_min.y) / f;
                float t2 = (e + aabb_max.y) / f;

                if (t1 > t2) {
                    float w = t1;
                    t1 = t2;
                    t2 = w;
                }

                if (t2 < tMax)
                    tMax = t2;
                if (t1 > tMin)
                    tMin = t1;
                if (tMin > tMax)
                    return false;

            } else {
                if (-e + aabb_min.y > 0.0f || -e + aabb_max.y < 0.0f)
                    return false;
            }
        }


        // Test intersection with the 2 planes perpendicular to the OBB's Z axis
        // Exactly the same thing than above.
        {
            vec3 zAxis(modelMatrix[2].x, modelMatrix[2].y, modelMatrix[2].z);
            float e = dot(zAxis, delta);
            float f = dot(rayDirection, zAxis);

            if (fabs(f) > 0.001f) {

                float t1 = (e + aabb_min.z) / f;
                float t2 = (e + aabb_max.z) / f;

                if (t1 > t2) {
                    float w = t1;
                    t1 = t2;
                    t2 = w;
                }

                if (t2 < tMax)
                    tMax = t2;
                if (t1 > tMin)
                    tMin = t1;
                if (tMin > tMax)
                    return false;

            } else {
                if (-e + aabb_min.z > 0.0f || -e + aabb_max.z < 0.0f)
                    return false;
            }
        }

        intersectionDistance = tMin;
        return true;
    }
};

#endif //RCCLANIMATION_OBBCOLLISION_H
