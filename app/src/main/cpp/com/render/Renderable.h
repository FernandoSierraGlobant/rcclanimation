//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_RENDEROBJECT_H
#define RCCLANIMATION_RENDEROBJECT_H

#include "../../PlatformProvider.h"
#include <vector>
#include "../../libs/glm/glm.hpp"
#include "../utils/OpenGLUtils.h"
#include "../utils/TextureManager.h"
#include "../animation/Animable.h"
#include "Shaders.h"
#include "cameras/Camera.h"
#include "lights/Light.h"
#include "lights/Sun.h"

using namespace std;
using namespace glm;

class Renderable : public Animable, public Shaders {
private:
    static int instancesCounter;

protected:
    int id;
    mat4 modelMatrix = mat4(1.0f); //Identity matrix
    vector<vec3> vertexes;
    vector<vec2> uvs;
    vector<vec3> normals;
    GLuint vertexesBufferId;
    GLuint uvsBufferId;
    GLuint normalsBufferId;
    vector<GLuint> texturesVector;
    GLuint vPositionId = 0;
    GLuint vNormalId = 1;
    GLuint vUvId = 2;
    GLint projectionMatrixId;
    GLint viewMatrixId;
    GLint modelMatrixId;
    GLint textureSamplerId;
    GLint viewPositionId;
    GLint lightPositionId;
    GLint ambientId;
    GLint diffuseId;
    GLint specularId;
    GLint shininessId;
    Light *worldLight = nullptr;

    void activateAndBindTexture(GLuint textureId) const;

    void activateAndBindVertexes() const;

    void activateAndBindUvs() const;

    void activateAndBindNormals() const;

    void activateAndBindLight(Light *light);

    void disableAttributes() const;

    virtual void loadTexture(const char *texturePath);

    virtual GLuint getTextureToDisplay();

    virtual const char *getVertexShader() override;

    virtual const char *getFragmentShader() override;

    virtual void linkAttributes() override;

    virtual void setMatrices(mat4 projectionMatrix);

    virtual void linkMatrices();

    virtual void linkLight();

    void linkPosition() const;

    void linkNormal() const;

    void linkUV() const;

    void linkTextureSampler();

public:

    Renderable();

    Light *getWorldLight() const;

    void setWorldLight(Light *worldLight);

    const mat4 &getModelMatrix() const;

    bool load(const char *objPath, const char *texturePath);

    virtual void draw(mat4 projectionMatrix);

    ~Renderable();
};


#endif //RCCLANIMATION_RENDEROBJECT_H
