//
// Created by jesus.sierra on 23/02/17.
//

#ifndef RCCLANIMATION_SHADERS_H
#define RCCLANIMATION_SHADERS_H

#include "../../PlatformProvider.h"

class Shaders {
protected:
    const char *V_POSITION = "v";
    const char *V_NORMAL = "vn";
    const char *V_UV = "vt";
    const char *TEXTURE_SAMPLER = "material.texture";
    const char *SHININESS = "material.shininess";
    const char *LIGHT_POSITION = "light.position";
    const char *AMBIENT = "light.ambient";
    const char *DIFFUSE = "light.diffuse";
    const char *SPECULAR = "light.specular";
    const char *VIEW_POSITION = "viewPosition";
    const char *MODEL_MATRIX = "model";
    const char *VIEW_MATRIX = "view";
    const char *PROJECTION_MATRIX = "projection";
    GLuint programId;

    virtual const char *getVertexShader() = 0;

    virtual const char *getFragmentShader() = 0;

private:
    GLuint vertexShaderId;
    GLuint fragmentShaderId;

    void checkShaderStatus(GLuint &shader) {
        GLint compiled;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLength = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLength);
            if (infoLength > 1) {
                char *infoLog = (char *) malloc(sizeof(char *) * infoLength);
                glGetShaderInfoLog(shader, infoLength, NULL, infoLog);
                logger->log(Logger::ERROR, infoLog);
                free(infoLog);
            }
            glDeleteShader(shader);
            shader = 0;
        }
    }

    GLuint loadShader(GLenum type, const char *shaderSrc) {
        GLuint shader;
        shader = glCreateShader(type);
        if (shader == 0) {
            return 0;
        }
        glShaderSource(shader, 1, &shaderSrc, NULL);
        glCompileShader(shader);
        checkShaderStatus(shader);
        return shader;
    }

    void checkProgramStatus() const {
        GLint linked;
        glGetProgramiv(programId, GL_LINK_STATUS, &linked);
        if (!linked) {
            GLint infoLength = 0;
            glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLength);
            if (infoLength > 1) {
                char *infoLog = (char *) malloc(sizeof(char) * infoLength);
                glGetProgramInfoLog(programId, infoLength, NULL, infoLog);
                logger->log(Logger::ERROR, infoLog);
                free(infoLog);
            }
        }
    }

    void createProgram() {
        programId = glCreateProgram();
        if (programId == 0) {
            logger->log(Logger::ERROR, "Program has not been created");
        } else {
            glAttachShader(programId, vertexShaderId);
            glAttachShader(programId, fragmentShaderId);
            glLinkProgram(programId);
            checkProgramStatus();
            linkAttributes();
        }
    }

    void releaseShaders() const {
        glDetachShader(programId, vertexShaderId);
        glDetachShader(programId, fragmentShaderId);
        glDeleteShader(vertexShaderId);
        glDeleteShader(fragmentShaderId);
    }

public:

    void initShaders() {
        vertexShaderId = loadShader(GL_VERTEX_SHADER, getVertexShader());
        fragmentShaderId = loadShader(GL_FRAGMENT_SHADER, getFragmentShader());
        createProgram();
        releaseShaders();
    }

    virtual ~Shaders() {
        glDeleteProgram(programId);
    }

    virtual void linkAttributes() = 0;
};

#endif //RCCLANIMATION_SHADERS_H
