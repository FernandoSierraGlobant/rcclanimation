//
// Created by jesus.sierra on 23/02/17.
//

#ifndef RCCLANIMATION_LIGHT_H
#define RCCLANIMATION_LIGHT_H

#include "../Renderable.h"

class Light {
protected:
    vec4 position;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;

public:
    Light() {};

    const vec4 &getPosition() const {
        return position;
    }

    void setPosition(const vec4 &position) {
        Light::position = position;
    }

    const vec4 &getAmbient() const {
        return ambient;
    }

    void setAmbient(const vec4 &ambient) {
        Light::ambient = ambient;
    }

    const vec4 &getDiffuse() const {
        return diffuse;
    }

    void setDiffuse(const vec4 &diffuse) {
        Light::diffuse = diffuse;
    }

    const vec4 &getSpecular() const {
        return specular;
    }

    void setSpecular(const vec4 &specular) {
        Light::specular = specular;
    }

    float getShininess() const {
        return shininess;
    }

    void setShininess(float shininess) {
        Light::shininess = shininess;
    }
};

#endif //RCCLANIMATION_LIGHT_H
