//
// Created by Jesús Fernando Sierra Pastrana on 14/02/17.
//

#ifndef RCCLANIMATION_CAMERA_H
#define RCCLANIMATION_CAMERA_H

#include "../../../libs/glm/glm.hpp"
#include "../../../libs/glm/gtc/matrix_transform.hpp"

using namespace glm;

class Camera {
private:
    static Camera *instance;
    vec3 position;
    vec3 target;
    const vec3 orientation = vec3(0.0f, 1.0f, 0.0f);
    mat4 viewMatrix;

    Camera() {
        setDefaultPosition();
        setDefaultTarget();
        update();
    }

public:
    static Camera *getInstance() {
        if (instance == nullptr) {
            instance = new Camera();
        }
        return instance;
    }

    const vec3 &getPosition() const {
        return position;
    }

    void setPosition(const vec3 &position) {
        Camera::position = position;
    }

    const vec3 &getTarget() const {
        return target;
    }

    void setTarget(const vec3 &target) {
        Camera::target = target;
    }

    const mat4 &getViewMatrix() const { return viewMatrix; }

    void setDefaultPosition() {
        position = vec3(0.0f, 0.0f, 3.0f);
    }

    void setDefaultTarget() {
        target = vec3(0.0f, 0.0f, 0.0f);
    }

    void update() {
        viewMatrix = lookAt(position, target, orientation);
    }
};

#endif //RCCLANIMATION_CAMERA_H
