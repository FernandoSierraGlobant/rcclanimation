//
// Created by Fernando on 11/02/2017.
//

#include "Renderable.h"

int Renderable::instancesCounter = 0;

Renderable::Renderable() {
    id = ++instancesCounter;
}

const char *Renderable::getVertexShader() {
    return "uniform mat4 model;\n"
            "uniform mat4 view;\n"
            "uniform mat4 projection;\n"
            "attribute vec3 v;\n"
            "attribute vec3 vn;\n"
            "attribute vec2 vt;\n"
            "varying vec2 UV;\n"
            "varying vec4 FragPos;\n"
            "varying vec4 Normal;\n\n"
            "mat4 inverse(mat4 m)\n"
            "{\n"
            "    float\n"
            "        a00 = m[0][0], a01 = m[0][1], a02 = m[0][2], a03 = m[0][3],\n"
            "        a10 = m[1][0], a11 = m[1][1], a12 = m[1][2], a13 = m[1][3],\n"
            "        a20 = m[2][0], a21 = m[2][1], a22 = m[2][2], a23 = m[2][3],\n"
            "        a30 = m[3][0], a31 = m[3][1], a32 = m[3][2], a33 = m[3][3],\n"
            "\n"
            "        b00 = a00 * a11 - a01 * a10,\n"
            "        b01 = a00 * a12 - a02 * a10,\n"
            "        b02 = a00 * a13 - a03 * a10,\n"
            "        b03 = a01 * a12 - a02 * a11,\n"
            "        b04 = a01 * a13 - a03 * a11,\n"
            "        b05 = a02 * a13 - a03 * a12,\n"
            "        b06 = a20 * a31 - a21 * a30,\n"
            "        b07 = a20 * a32 - a22 * a30,\n"
            "        b08 = a20 * a33 - a23 * a30,\n"
            "        b09 = a21 * a32 - a22 * a31,\n"
            "        b10 = a21 * a33 - a23 * a31,\n"
            "        b11 = a22 * a33 - a23 * a32,\n"
            "\n"
            "    det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;\n"
            "\n"
            "    return mat4(\n"
            "        a11 * b11 - a12 * b10 + a13 * b09,\n"
            "        a02 * b10 - a01 * b11 - a03 * b09,\n"
            "        a31 * b05 - a32 * b04 + a33 * b03,\n"
            "        a22 * b04 - a21 * b05 - a23 * b03,\n"
            "        a12 * b08 - a10 * b11 - a13 * b07,\n"
            "        a00 * b11 - a02 * b08 + a03 * b07,\n"
            "        a32 * b02 - a30 * b05 - a33 * b01,\n"
            "        a20 * b05 - a22 * b02 + a23 * b01,\n"
            "        a10 * b10 - a11 * b08 + a13 * b06,\n"
            "        a01 * b08 - a00 * b10 - a03 * b06,\n"
            "        a30 * b04 - a31 * b02 + a33 * b00,\n"
            "        a21 * b02 - a20 * b04 - a23 * b00,\n"
            "        a11 * b07 - a10 * b09 - a12 * b06,\n"
            "        a00 * b09 - a01 * b07 + a02 * b06,\n"
            "        a31 * b01 - a30 * b03 - a32 * b00,\n"
            "        a20 * b03 - a21 * b01 + a22 * b00) / det;\n"
            "}\n\n"
            "mat4 transpose(mat4 m)\n"
            "{\n"
            "    return mat4(m[0][0], m[1][0], m[2][0], m[3][0],\n"
            "                m[0][1], m[1][1], m[2][1], m[3][1],\n"
            "                m[0][2], m[1][2], m[2][2], m[3][2],\n"
            "                m[0][3], m[1][3], m[2][3], m[3][3]);\n"
            "}\n\n"
            "void main()\n"
            "{\n"
            "    UV = vt;\n"
            "    vec4 position = vec4(v, 1.0);\n"
            "    FragPos = model * position;\n"
            "    Normal = normalize(transpose(inverse(model)) * vec4(vn, 0.0));\n"
            "    gl_Position = projection * view * model * position;\n"
            "}";
}

const char *Renderable::getFragmentShader() {
    return "precision mediump float;\n"
            "struct Material {\n"
            "    sampler2D texture;\n"
            "    float shininess;\n"
            "};\n"
            "struct Light {\n"
            "    vec4 position;\n"
            "    vec4 ambient;\n"
            "    vec4 diffuse;\n"
            "    vec4 specular;\n"
            "};\n"
            "uniform Light light;\n"
            "uniform Material material;\n"
            "uniform vec4 viewPosition;\n"
            "varying vec2 UV;\n"
            "varying vec4 FragPos;\n"
            "varying vec4 Normal;\n"
            "void main()\n"
            "{\n"
            "    vec4 tex = texture2D(material.texture, UV);\n"
            "    // Ambient\n"
            "    vec4 ambient = light.ambient * tex;\n"
            "    // Diffuse\n"
            "    vec4 lightDir = normalize(light.position - FragPos);\n"
            "    float diff = max(dot(Normal, lightDir), 0.0);\n"
            "    vec4 diffuse = light.diffuse * diff * tex;\n"
            "    // Specular\n"
            "    vec4 viewDir = normalize(viewPosition - FragPos);\n"
            "    vec4 reflectDir = reflect(-lightDir, Normal);\n"
            "    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);\n"
            "    vec4 specular = light.specular * spec * tex;\n"
            "    gl_FragColor = ambient + diffuse + specular;\n"
            "}";
}

void Renderable::linkAttributes() {
    linkPosition();
    linkNormal();
    linkUV();
    linkMatrices();
    linkTextureSampler();
    linkLight();
}

void Renderable::loadTexture(const char *texturePath) {
    vector<const char *> textures;
    textures.push_back(texturePath);
    TextureManager::loadTextures(id, textures);
    texturesVector = TextureManager::getTexturesList(id);
}

bool Renderable::load(const char *objPath,
                      const char *texturePath) {
    bool result = OpenGLUtils::loadObj(objPath, vertexes, normals, uvs);
    vertexesBufferId = OpenGLUtils::createBuffer(vertexes);
    normalsBufferId = OpenGLUtils::createBuffer(normals);
    uvsBufferId = OpenGLUtils::createBuffer(uvs);
    loadTexture(texturePath);
    initShaders();
    return result;
}

void Renderable::activateAndBindUvs() const {
    glEnableVertexAttribArray(vUvId);
    glBindBuffer(GL_ARRAY_BUFFER, uvsBufferId);
    glVertexAttribPointer(vUvId, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
}

void Renderable::activateAndBindVertexes() const {
    glEnableVertexAttribArray(vPositionId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexesBufferId);
    glVertexAttribPointer(vPositionId, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
}

void Renderable::activateAndBindNormals() const {
    glEnableVertexAttribArray(vNormalId);
    glBindBuffer(GL_ARRAY_BUFFER, normalsBufferId);
    glVertexAttribPointer(vNormalId, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
}

void Renderable::activateAndBindLight(Light *light) {
    auto viewPosition = Camera::getInstance()->getPosition();
    auto position = light->getPosition();
    auto ambient = light->getAmbient();
    auto diffuse = light->getDiffuse();
    auto specular = light->getSpecular();

    glUniform4f(viewPositionId, viewPosition.x, viewPosition.y, viewPosition.z, 1.0f);
    glUniform4f(lightPositionId, position.x, position.y, position.z, position.w);
    glUniform4f(ambientId, ambient.x, ambient.y, ambient.z, ambient.w);
    glUniform4f(diffuseId, diffuse.x, diffuse.y, diffuse.z, diffuse.w);
    glUniform4f(specularId, specular.x, specular.y, specular.z, specular.w);
    glUniform1f(shininessId, light->getShininess());
}

void Renderable::activateAndBindTexture(GLuint textureId) const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glUniform1i(textureSamplerId, 0);
}

void Renderable::disableAttributes() const {
    glDisableVertexAttribArray(vPositionId);
    glDisableVertexAttribArray(vUvId);
    glDisableVertexAttribArray(vNormalId);
}

void Renderable::draw(mat4 projectionMatrix) {
    glUseProgram(programId);

    setMatrices(projectionMatrix);

    if (animation != nullptr) {
        animation->transform();
    }
    activateAndBindTexture(getTextureToDisplay());
    activateAndBindVertexes();
    activateAndBindUvs();
    activateAndBindNormals();
    activateAndBindLight(worldLight);

    glDrawArrays(GL_TRIANGLES, 0, (GLsizei) vertexes.size());

    disableAttributes();
}

const mat4 &Renderable::getModelMatrix() const {
    return modelMatrix;
}

Renderable::~Renderable() {
    Shaders::~Shaders();
    glDeleteBuffers(1, &vertexesBufferId);
    glDeleteBuffers(1, &uvsBufferId);
    glDeleteBuffers(1, &normalsBufferId);
    TextureManager::clear(id);
}

GLuint Renderable::getTextureToDisplay() {
    return texturesVector.empty() ? 0 : texturesVector[0];
}

void Renderable::linkTextureSampler() {
    textureSamplerId = glGetUniformLocation(programId, TEXTURE_SAMPLER);
}

void Renderable::linkUV() const { glBindAttribLocation(programId, vUvId, V_UV); }

void Renderable::linkPosition() const { glBindAttribLocation(programId, vPositionId, V_POSITION); }

void Renderable::linkNormal() const { glBindAttribLocation(programId, vNormalId, V_NORMAL); }

void Renderable::setMatrices(mat4 projectionMatrix) {
    glUniformMatrix4fv(projectionMatrixId, 1, GL_FALSE, &projectionMatrix[0][0]);
    glUniformMatrix4fv(viewMatrixId, 1, GL_FALSE, &Camera::getInstance()->getViewMatrix()[0][0]);
    glUniformMatrix4fv(modelMatrixId, 1, GL_FALSE, &modelMatrix[0][0]);
}

void Renderable::linkMatrices() {
    modelMatrixId = glGetUniformLocation(programId, MODEL_MATRIX);
    viewMatrixId = glGetUniformLocation(programId, VIEW_MATRIX);
    projectionMatrixId = glGetUniformLocation(programId, PROJECTION_MATRIX);
}

void Renderable::linkLight() {
    viewPositionId = glGetUniformLocation(programId, VIEW_POSITION);
    lightPositionId = glGetUniformLocation(programId, LIGHT_POSITION);
    ambientId = glGetUniformLocation(programId, AMBIENT);
    diffuseId = glGetUniformLocation(programId, DIFFUSE);
    specularId = glGetUniformLocation(programId, SPECULAR);
    shininessId = glGetUniformLocation(programId, SHININESS);
}

Light *Renderable::getWorldLight() const {
    return worldLight;
}

void Renderable::setWorldLight(Light *worldLight) {
    Renderable::worldLight = worldLight;
}
