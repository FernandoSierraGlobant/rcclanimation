//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_WORLD_H
#define RCCLANIMATION_WORLD_H

#include "items/Water.h"
#include "items/Land.h"
#include "items/Ship.h"
#include "../utils/OBBCollision.h"

class World {
private:
    static World *instance;
    mat4 projectionMatrix;
    Camera *camera = nullptr;
    Water *water = nullptr;
    Land *land = nullptr;
    Ship *ship = nullptr;
    Sun *sun = nullptr;
    int screenWidth;
    int screenHeight;

    World();

    void draw(Renderable *renderable);

public:
    static World *getInstance();

    void setProjectionMatrix(int screenWidth, int screenHeight);

    void setWater(Water *water);

    Water *getWater() const;

    void setLand(Land *land);

    void setShip(Ship *ship);

    Ship *getShip() const;

    void draw();

    ~World();

    int click(float x, float y);
};


#endif //RCCLANIMATION_WORLD_H
