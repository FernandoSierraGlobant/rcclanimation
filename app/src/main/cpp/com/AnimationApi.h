//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_ANIMATIONAPI_H
#define RCCLANIMATION_ANIMATIONAPI_H

#include "../PlatformProvider.h"
#include "render/World.h"

using namespace glm;

class AnimationApi {
private:
    World *world = World::getInstance();

public:
    void initOpenGL();

    void setViewPort(int width, int height);

    bool loadWater(const char *objPath, const char *texturePath);

    bool loadShip(const char *objPath, const char *texturePath);

    void draw();

    int click(float x, float y);

    ~AnimationApi();
};


#endif //RCCLANIMATION_ANIMATIONAPI_H
