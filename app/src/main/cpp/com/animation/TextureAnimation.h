//
// Created by Jesús Fernando Sierra Pastrana on 16/02/17.
//

#ifndef RCCLANIMATION_TEXTUREANIMATION_H
#define RCCLANIMATION_TEXTUREANIMATION_H

#include "Animation.h"
#include <math.h>

class TextureAnimation : public Animation {
private:
    unsigned long texturesTotal;
    int currentFrameTexture;

protected:
    virtual void animateStep(double step) {
        currentFrameTexture = (int) round((step) * (texturesTotal - 1));
    }

public:
    void setTexturesTotal(unsigned long texturesCount) {
        TextureAnimation::texturesTotal = texturesCount;
    }

    int getCurrentTexturePosition() { return currentFrameTexture; }
};


#endif //RCCLANIMATION_TEXTUREANIMATION_H
