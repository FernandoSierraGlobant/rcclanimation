//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_ANDROIDLOGGER_H_H
#define RCCLANIMATION_ANDROIDLOGGER_H_H

#include "../com/utils/Logger.h"
#include <android/log.h>

class AndroidLogger : public Logger {
private:
    int getPriority(LogLevel level) {
        int priority;
        switch (level) {
            case LogLevel::ERROR:
                priority = ANDROID_LOG_ERROR;
                break;
            case LogLevel::INFO:
                priority = ANDROID_LOG_INFO;
                break;
            case LogLevel::VERBOSE:
                priority = ANDROID_LOG_VERBOSE;
                break;
            default:
                priority = ANDROID_LOG_DEBUG;
        }
        return priority;
    }

public:
    virtual void log(LogLevel level, const char *message) override {
        __android_log_print(getPriority(level), TAG_LOG, "%s", message);
    }
};

#endif //RCCLANIMATION_ANDROIDLOGGER_H_H
